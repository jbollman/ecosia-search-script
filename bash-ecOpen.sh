#!/bin/bash
COUNTER=1
while IFS='' read -r line || [[ -n "$line" ]]; do
	echo "Text read from file: $line"
	xdg-open http://www.ecosia.org/search?q=$line;
	sleep 6; 
	 echo " $COUNTER "
     	let COUNTER++
	if [ "$COUNTER" -gt "11" ]
	then
		pkill -f firefox
		sleep 26
		COUNTER=0
	fi
done < "$1"

